##------------------------------------------------------------------------------##
## The following script is for a nested ANOVA.                                  ##
## Used when you have one measurement variable and two or more nominal          ##
## variables, where at least one factor is nested inside another.               ##
##------------------------------------------------------------------------------##

################################## Loading in the data and packages########################

#Load packages. First time install packages using e.g., install.packages("magrittr")

#load packages
library(magrittr)
library(dplyr)

## set the working directory where the data file is stored ####
setwd("/Users/....")


################################### Preparing the data ##################
#read in the data file in the appropriate format 
## see Nested_ANOVA_data file to view example of the data format
data <- read.table ("Nested_ANOVA_data.csv", sep=",", header=T)

#check data has read in properly
data

#Make sure that variables are in the correct data type and change where applicable.
str(data)
data$Sample.id <- as.factor(data$Sample.id)
is.factor(data$Sample)

################################### plot univariate effects ###################

##plot the univariate effect graphically to explore the distribution of values for a single variable.

#set the dimensions of the plot (mar = margins and these correspond to par(mar = c(bottom, left, top, right))
par(mar = c(1, 4.4, 1, 1.9))
#plot univariate effect 
plot.design(data, pch=4, cex=0.5)


################################### plot two way interactions ###############
#investigate the data further by investigating which combinations of factors alters the mean recordings the most

tw <- par(mfrow = c(3, 1))
with(data, {
  interaction.plot(Treatment, Islet, Recording)
  interaction.plot(Treatment, Sample.id, Recording)
  interaction.plot(Treatment, Days.in.culture, Recording)
}
)
par(tw)

### to plot a clearer graph for a factor ###

par(mfrow=c(1,1))
tw2<-interaction.plot(data$Treatment, data$Islet, data$Recording, fun=mean,
                      type="b", legend = F, lty=c(1,2), lwd=2, 
                      xlab = "Treatment",
                      ylab = "Mean of recording",
                      ylim=c(30, 80),
                      col=terrain.colors(13))

################################ 3-way ANOVA ##########################

fwa <- aov(Recording ~ Treatment* Sample.id * Days.in.culture, data = data)
summary(fwa)

###### table of effects from the nested ANOVA ######
model.tables(fwa, type ="effects")

###### table of means from the nested ANOVA ######
model.tables(fwa, type="means")

##### obtain the model with significant effects only #####
##nb: if you have more than one condition which is significant the code would be: 
#fwa2<-aov(Recordings~Treatment+Patient.donor, data=hi_data)
fwa2<-aov(Recording~Treatment, data = data)
summary(fwa2)

################ residuals analysis of 3 way anova #########
ra<-par(mfrow = c(2,2))
plot(fwa2)
par(ra)

########## obtain recordings means for the treatment #########
data %>% group_by(Treatment) %>% summarise(mean(Recording)) 


