# Biostatistics

## Name
R scripts for biostatistical analysis

## Description
The ‘Scripts’ directory houses R scripts designed for the statistical analysis of a range of experimental data. The ‘Data’ directory includes sample data demonstrating the input format accepted by each script.

## Usage
The contained R scripts are designed for those new to the models and serve as an introductory guide to both the concepts and R code syntax for analysing a range of experimental data.

## Authors and acknowledgment
I extend my gratitude to the R community for open source code, and to the mathematicians who have generously dedicated their time to teach me the underlying concepts. 

## Project status
This is an ongoing project to share methods for analysing data with the experimentalist community.